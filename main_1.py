import time

from context.bot.domain.interfaces.bot_interface import BotInterface

bot_interface = BotInterface(
    b_id=1,
    name="botinterface")

bot_interface.start()

time.sleep(5)

bot_interface.stop()