import os
import time
import uuid

from termcolor import colored
from context.strategy.domain.entities.action import Action
from context.wallet.domain.usecases.buy_asset import BuyAsset
from context.wallet.domain.usecases.sell_asset import SellAsset
from context.bot.domain.interfaces.bot_interface import BotInterface

class BotSupportResistance(BotInterface):

	def __init__(self, b_id, name, repository, strategy, wallet):
		self.repository = repository
		self.strategy = strategy
		self.wallet = wallet
		self.buy_usecase = BuyAsset()
		self.sell_usecase = SellAsset()
		super(BotSupportResistance, self).__init__(b_id, name)

	def start(self):
		while True:
			#os.system('reset')
			self._operate()
			print(self.wallet, flush=True)
			time.sleep(0)

	def _operate(self):
		# Get currency
		currency = self.repository.get("BTC")
		# Evaluate currency and evaluate action (buy, sell or nothing)
		action = self.strategy.evaluate(currency.last)
		operation = None
		# If action is buy and has free resources then ...
		if action == Action.BUY and self.wallet.has_free_resources():
			if self.wallet.resources > 200.00:
				amount = 200.0

			else:
				amount = round(self.wallet.resources, 2)

			self.wallet.remove_resources(amount)

			operation = self.buy_usecase.buy(
				wallet=self.wallet,
				ticker=currency.ticker,
				qnt=round(amount/currency.last, 8),
				price=currency.last,
				timestamp=currency.timestamp)
		# If action is sell and has position then ...
		elif action == Action.SELL and self.wallet.has_position(currency.ticker):
			qnt = self.wallet.get_asset(currency.ticker).qnt

			self.wallet.add_resources(round(qnt * currency.last, 2))

			operation = self.sell_usecase.sell(
				wallet=self.wallet,
				ticker=currency.ticker,
				qnt=qnt,
				price=currency.last,
				timestamp=currency.timestamp)
		# If action is nothing pass ...
		elif action == Action.NOTHING:
			pass

		print(colored("Action: ", 'blue'), colored(action, 'green'))
		print(operation)

	def stop(self):
		print("Stop")