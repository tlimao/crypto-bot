import os
import time
import uuid
from datetime import datetime
from termcolor import colored

from context.crypto.domain.entities.bitcoin import Bitcoin
from context.strategy.domain.entities.action import Action
from context.bot.domain.interfaces.bot_interface import BotInterface
from context.wallet.domain.usecases.buy_asset import BuyAsset
from context.wallet.domain.usecases.sell_asset import SellAsset
from context.wallet.errors.trade_operation_errors import AssetNotInWallet

class BotRsi(BotInterface):

	def __init__(self, b_id, name, repository, strategy, wallet):
		self.repository = repository
		self.strategy = strategy
		self.wallet = wallet
		self.buy_usecase = BuyAsset()
		self.sell_usecase = SellAsset()
		self.buy_count = 0
		self.last_buy = None
		self.last_currency = None
		super(BotRsi, self).__init__(b_id, name)

	def operate(self):
		try:
			currency = self.repository.get(Bitcoin.ticker)
			self.last_currency = currency
			rsi_value = self.strategy.evaluate(currency.last)
			operation = None
			action = self._eval_indicator(rsi_value, currency) 

			if action == Action.BUY:
				if self.buy_count < 6:
					if self.wallet.resources > 40.00:
						amount = round(40.00, 2)

					else:
						amount = round(self.wallet.resources, 2)

					self.wallet.remove_resources(amount)

					self.last_buy = currency.timestamp
					self.buy_count += 1

					operation = self.buy_usecase.buy(
						wallet=self.wallet,
						ticker=currency.ticker,
						qnt=round(amount/currency.last, 8),
						price=currency.last,
						timestamp=currency.timestamp)
					
					self.print_operation(action, operation)
				
				self.update_buy_count(currency.timestamp)

			elif action == Action.SELL:
				qnt = self.rebalance(currency) #self.wallet.get_asset(currency.ticker).qnt

				self.wallet.add_resources(round(qnt * currency.last, 2))

				operation = self.sell_usecase.sell(
					wallet=self.wallet,
					ticker=currency.ticker,
					qnt=qnt,
					price=currency.last,
					timestamp=currency.timestamp)

				self.print_operation(action, operation)

			return action, operation, currency, rsi_value

		except AssetNotInWallet as e:
			print(e, flush=True)

		except Exception as e:
			print(e)
			print(self.wallet)
			print(self.last_currency)
			print(self.result())
			self.stop()

	def _eval_indicator(self, value, currency):
		if value != None:
			if value >= 70.0 and self.wallet.has_position(currency.ticker):
				return Action.SELL
			
			elif value <= 30.0 and self.wallet.has_free_resources():
				return Action.BUY
			
			else:
				return Action.NOTHING
		else:
			return Action.NOTHING

	def rebalance(self, currency):
		position = self.wallet.get_asset(currency.ticker)
		now_value = position.qnt * currency.last
		sell_qnt = 0

		if now_value > self.wallet.deposits:
			sell_qnt = position.qnt * ((now_value - self.wallet.deposits)/self.wallet.deposits)

		return sell_qnt

	def print_operation(self, action, operation):
		print(colored("Action: ", 'blue'), colored(action, 'green'))
		print(operation, flush=True)
	
	def update_buy_count(self, timestamp):
		offset = timestamp - self.last_buy
		self.buy_count -= offset.days

		if self.buy_count < 0:
			self.buy_count = 0

	def result(self):
		result = self.wallet.resources

		assets = self.wallet.get_assets()

		for key, value in assets.items():
			if value.qnt != 0:
				result += value.qnt * self.last_currency.last

		return result