import os
import time
import uuid
from datetime import datetime

from termcolor import colored
from context.strategy.domain.entities.action import Action
from context.wallet.domain.usecases.buy_asset import BuyAsset
from context.wallet.domain.usecases.sell_asset import SellAsset
from context.bot.domain.interfaces.bot_interface import BotInterface
from context.bot.view.waiting_cli import WaitingCli

class BotMovingAverage(BotInterface):

    def __init__(self, b_id, name, repository, strategy, wallet):
        self.repository = repository
        self.strategy = strategy
        self.wallet = wallet
        self.buy_usecase = BuyAsset()
        self.sell_usecase = SellAsset()
        self.count = 0
        self.buy_count = 0
        self.last_buy = None
        self.waiting = WaitingCli()
        self.history = {"prices": [], "mm_values": []}
        super(BotMovingAverage, self).__init__(b_id, name)

    def start(self):
        while True:
            self._operate()
            if self.count < 14:
                self.count += 1
                #time.sleep(5)
            else:
                #time.sleep(30)
                pass

    def _operate(self):
        currency = self.repository.get("BTC")
        mm_value = self.strategy.evaluate(currency.last)
        operation = None
        action = None

        if len(self.history["mm_values"]) == 2 and self.history["mm_values"][0] != None and self.history["mm_values"][1] != None:
            last_mm_value = self.history["mm_values"][0] 
            new_mm_value = mm_value

            last_price = self.history["prices"][0]
            new_price = currency.last

            if last_mm_value < last_price:
                if new_mm_value > new_price:
                    action = Action.SELL
                
                else:
                    action = Action.NOTHING
            else:
                if new_mm_value > new_price:
                    action = Action.NOTHING
                
                else:
                    action = Action.BUY

        self.history["prices"].append(currency.last)
        self.history["mm_values"].append(mm_value)

        if len(self.history["prices"]) > 2:
            self.history["prices"] = self.history["prices"][1:]
            self.history["mm_values"] = self.history["mm_values"][1:]

        if action == Action.BUY and self.wallet.has_free_resources():
            if self.buy_count < 6:
                if self.wallet.resources > 40.00:
                    amount = round(40.00, 2)

                else:
                    amount = round(self.wallet.resources, 2)

                self.wallet.remove_resources(amount)

                self.last_buy = currency.timestamp
                self.buy_count += 1

                operation = self.buy_usecase.buy(
                    wallet=self.wallet,
                    ticker=currency.ticker,
                    qnt=round(amount/currency.last, 8),
                    price=currency.last,
                    timestamp=currency.timestamp)
                
                # self.print_operation(action, operation)
            
            self.update_buy_count(currency.timestamp)

        elif action == Action.SELL and self.wallet.has_position(currency.ticker):
            qnt = self.wallet.get_asset(currency.ticker).qnt

            self.wallet.add_resources(round(qnt * currency.last, 2))

            operation = self.sell_usecase.sell(
                wallet=self.wallet,
                ticker=currency.ticker,
                qnt=qnt,
                price=currency.last,
                timestamp=currency.timestamp)

            # self.print_operation(action, operation)
        
        return action, operation, currency, mm_value
    
    def print_operation(self, action, operation):
        print(colored("Action: ", 'blue'), colored(action, 'green'))
        print(operation, flush=True)
    
    def update_buy_count(self, timestamp):
        offset = timestamp - self.last_buy
        self.buy_count -= offset.days

        if self.buy_count < 0:
            self.buy_count = 0

        print(self.buy_count)

    def stop(self):
        print("Stop", flush=True)