import os
import time
import yaml
from datetime import datetime

from examples.bot_2 import BotRsi
from context.wallet.domain.entities.wallet import Wallet
from context.strategy.domain.entities.rsi import Rsi
from context.crypto.data.biscoint import BiscointExchange
from context.crypto.data.backtest import BackTest
from context.crypto.infra.biscoint_interface_impl import BiscointInterfaceImpl
from context.crypto.infra.backtest_interface_impl import BackTestInterfaceImpl

if __name__ == "__main__":
	strategy = Rsi(period=14)
	api_keys = None

	with open(r'config-dev.yaml') as file:
		api_keys = yaml.full_load(file)

	data = BiscointExchange(
		api_keys['biscoint']['public-key'],
		api_keys['biscoint']['secret-key'])

	# repository = BiscointInterfaceImpl(data)

	repository = BackTestInterfaceImpl(
		BackTest(),
		datetime.strptime("2018-04-01", "%Y-%m-%d"),
		datetime.strptime("2021-01-24", "%Y-%m-%d"))

	wallet = Wallet("wallet_2", "Wallet Test", resources=10000.00)

	bot_2 = BotRsi(
		"2",
		"Relative Strength Indicator (RSI)",
		repository,
		strategy,
		wallet)

	bot_2.start()