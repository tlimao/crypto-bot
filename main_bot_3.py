import os
import time
import yaml
from datetime import datetime

from examples.bot_3 import BotMovingAverage
from context.wallet.domain.entities.wallet import Wallet
from context.strategy.domain.entities.moving_average import MovingAverage
from context.crypto.data.biscoint import BiscointExchange
from context.crypto.data.backtest import BackTest
from context.crypto.infra.biscoint_interface_impl import BiscointInterfaceImpl
from context.crypto.infra.backtest_interface_impl import BackTestInterfaceImpl

if __name__ == "__main__":
    strategy = MovingAverage(period=14)
    api_keys = None

    with open(r'config-dev.yaml') as file:
        api_keys = yaml.full_load(file)

    data = BiscointExchange(
		api_keys['biscoint']['public-key'],
		api_keys['biscoint']['secret-key']

    # repository = BiscointInterfaceImpl(data)

    repository = BackTestInterfaceImpl(
        BackTest(),
        datetime.strptime("2018-04-01", "%Y-%m-%d"),
        datetime.strptime("2020-12-01", "%Y-%m-%d"))

    wallet = Wallet("wallet_3", "Wallet Test", resources=10000.00)

    bot_3 = BotMovingAverage(
        "3",
        "Bot to Moving Average (MM)",
        repository,
        strategy,
        wallet)

    try:
        bot_3.start()

    except Exception as e:
        print(e, flush=True)
        print(wallet, flush=True)
        bot_3.stop()