import threading

class BotInterface(threading.Thread):

	def __init__(self, b_id, name):
		super(BotInterface, self).__init__()
		self._stop_event = threading.Event()

		self.id = b_id
		self.name = name

	def stop(self):
		self._stop_event.set()
	
	def stopped(self):
		return self._stop_event.is_set()

	def run(self):
		while True:
			self.operate()
			if self.stopped():
				print(f"{self} Stopped!")
				break

	def operate(self):
		raise Exception("Method not implemented!!")

	def __repr__(self):
		return f'Bot {self.id} {self.name}'