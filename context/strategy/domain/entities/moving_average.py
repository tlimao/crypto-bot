class MovingAverage:
    NAME = "Moving Average"

    def __init__(self, period=10):
        self.period = period
        self.value = None
        self.window = []

    def evaluate(self, price):
        self._update(price)

        return self.value

    def _update(self, new_price):
        if len(self.window) < self.period:
            self.window.append(new_price)

        else:
            self.value = sum(self.window)/self.period
            self.window = self.window[1:]
            self.window.append(new_price)
    
    def reset(self):
        self.window.clear()
        self.value = None

    def __repr__(self):
        return f'{self.NAME} ({self.period}): {self.value}'