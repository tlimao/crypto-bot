class Rsi:
    NAME = "Relative Strength Indicator"

    def __init__(self, period=10):
        self.period = period
        self.value = None
        self.window = []

    def evaluate(self, price):
        self._update(price)

        return self.value
    
    def _update(self, new_price):
        if len(self.window) < self.period - 1:
            self.window.append(new_price)
        
        else:
            self.window.append(new_price)
            rs = 0.0 # Relatice Strength
            down_moves = []
            up_moves = []
            avg_u = 0.0 # average of all up moves in the last N (period) price bars
            avg_d = 0.0 # average of all down moves in the last N (period) price bars

            for i in range(1, self.period):
                p0 = self.window[i-1]
                p1 = self.window[i]

                if p0 - p1 > 0:
                    down_moves.append(p1)
                else:
                    up_moves.append(p1)

            avg_d = sum(down_moves) / self.period
            avg_u = sum(up_moves) / self.period

            if avg_d != 0:
                rs = avg_u / avg_d
                self.value = 100.0 - (100.0 / (1 + rs))
            else:
                self.value = 100.0

            self.window = self.window[1:]

    def reset(self):
        self.window.clear()
        self.value = None
    
    def __repr__(self):
        return f'{self.NAME} (W = {self.period}): {self.value}'
