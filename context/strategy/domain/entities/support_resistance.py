from context.strategy.domain.entities.action import Action

class SupportResistance:

	def __init__(self):
		self.name = "Support Resistance Strategy"
		self.resistance_price = 0.0
		self.support_price = 0.0
		self.window = []
		self.window_size = 10

	def evaluate(self, price):
		if len(self.window) < self.window_size:
			self._update(price)

			return Action.NOTHING

		else:
			if price > self.resistance_price:
				return Action.BUY

			else:
				return Action.NOTHING

	def _update(self, price):
		if len(self.window) < self.window_size:
			self.window.append(price)

		else:
			self.window = (self.window[1:]).append(price)

		self.resistance_price = price if (price > self.resistance_price) else self.resistance_price
		self.support_price = price if (price < self.support_price) else self.support_price

	def __repr__(self):
		return f'{self.name}'