import json
from datetime import datetime

class TradeOperation:

	def __init__(self, t_type, t_id, ticker, qnt, price, timestamp):
		self.type = t_type
		self.id = t_id
		self.ticker = ticker
		self.qnt = qnt
		self.price = price
		self.timestamp = timestamp

	def __repr__(self):
		dict_copy = self.__dict__.copy()
		dict_copy["timestamp"] = datetime.strftime(dict_copy["timestamp"], "%Y-%m-%d %H:%M:%S")
		return json.dumps(dict_copy, indent=4)