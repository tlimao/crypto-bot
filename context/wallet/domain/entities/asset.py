import json

class Asset:

	def __init__(self, ticker, qnt=None, mean_price=None):
		self.ticker = ticker
		self.qnt = round(qnt, 8)
		self.mean_price = round(mean_price, 2)

	def __repr__(self):
		return json.dumps(self.__dict__)
