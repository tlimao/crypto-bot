from termcolor import colored
from context.wallet.errors.trade_operation_errors import WalletHasNoThatAmount
from context.wallet.errors.trade_operation_errors import AssetNotInWallet

class Wallet:

	def __init__(self, w_id, w_name, resources = 0.0):
		self.name = w_name
		self.id = w_id
		self.assets = {}
		self.deposits = resources
		self.resources = resources

	def has_free_resources(self):
		if self.resources > 0:
			return True
		
		else:
			return False
	
	def has_position(self, ticker):
		try:
			position = self.get_asset(ticker).qnt
			return True if position > 0 else False
		
		except AssetNotInWallet as e:
			return False

	def add_resources(self, amount):
		self.deposits += amount
		self.resources += amount
		self.resources = round(self.resources, 2)

	def remove_resources(self, amount):
		if self.resources >= amount:
			self.deposits -= amount
			self.resources -= amount
			self.resources = round(self.resources, 2)

			return amount

		else:
			raise WalletHasNoThatAmount()

	def withdraw_resources(self, amount):
		if self.resources >= amount:
			self.deposits -= amount
			self.resources -= amount
			self.resources = round(self.resources, 2)

			return amount

		else:
			raise WalletHasNoThatAmount()

	def add_asset(self, asset):
		self.assets[asset.ticker] = asset

	def get_asset(self, ticker):
		try:
			asset = self.assets[ticker]
			return asset
		except:
			raise AssetNotInWallet()

	def get_assets(self):
		return self.assets

	def remove_asset(self, ticker):
		del self.assets[ticker]

	def update_asset(self, asset):
		try:
			self.assets[asset.ticker] = asset
		except Exception as e:
			print(self.__class__.__name__, e)
	
	def resume(self, ticker):
		asset = self.get_asset(ticker)

		return asset

	def __repr__(self):
		wallet = colored(f"Wallet Id: {self.id}\nWallet Name: {self.name}\nResources: {self.resources}\n", 'blue')

		for key, value in self.assets.items():
			if value.qnt != 0:
				wallet += colored(f"  - {key} : {value.qnt} {value.mean_price}\n", 'yellow')

		return wallet