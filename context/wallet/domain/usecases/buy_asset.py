import uuid
from datetime import datetime

from context.wallet.domain.entities.asset import Asset
from context.wallet.domain.entities.trade_type import TradeType
from context.wallet.domain.entities.trade_operation import TradeOperation
from context.wallet.errors.trade_operation_errors import AssetNotInWallet

class BuyAsset:

	def buy(self, wallet, ticker, qnt, price, timestamp):
		try:
			position = wallet.get_asset(ticker)
			
			new_qnt = qnt + position.qnt
			new_mean_price = (position.qnt * position.mean_price + qnt * price) / new_qnt
			
			asset_updated = Asset(
				ticker=ticker,
				qnt=new_qnt,
				mean_price=new_mean_price)
				
			wallet.update_asset(asset_updated)
			
			trade_operation = TradeOperation(
				t_type=TradeType.BUY,
				t_id=str(uuid.uuid1()),
				ticker=ticker,
				qnt=qnt,
				price=price,
				timestamp=timestamp)

			return trade_operation

		except AssetNotInWallet:
			asset_new = Asset(
				ticker=ticker,
				qnt=qnt,
				mean_price=price)

			wallet.add_asset(asset_new)

			trade_operation = TradeOperation(
				t_type=TradeType.BUY,
				t_id=str(uuid.uuid1()),
				ticker=ticker,
				qnt=qnt,
				price=price,
				timestamp=timestamp)

			return trade_operation
		
		except Exception as e:
			print(self.__class__.__name__, e)