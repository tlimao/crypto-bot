import uuid

from context.wallet.domain.entities.asset import Asset
from context.wallet.domain.entities.trade_operation import TradeOperation

class RemoveAsset:

	def remove(self, wallet, ticker):
		try:
			position = wallet.get_asset(ticker)
			
			if position.qnt == 0:
				wallet.remove_asset(ticker)

		except Exception as e:
			print(self.__class__.__name__, " Asset not present in wallet")