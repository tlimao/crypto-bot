import uuid
from datetime import datetime

from context.wallet.domain.entities.asset import Asset
from context.wallet.domain.entities.trade_type import TradeType 
from context.wallet.domain.entities.trade_operation import TradeOperation

class SellAsset:

	def sell(self, wallet, ticker, qnt, price, timestamp):
		try:
			position = wallet.get_asset(ticker)
			new_qnt = position.qnt - qnt

			asset_updated = Asset(
				ticker=ticker,
				qnt=new_qnt,
				mean_price=position.mean_price)

			wallet.update_asset(asset_updated)

			trade_operation = TradeOperation(
				t_type=TradeType.SELL,
				t_id=str(uuid.uuid1()),
				ticker=ticker,
				qnt=qnt,
				price=price,
				timestamp=timestamp)

			return trade_operation

		except Exception as e:
			print(self.__class__.__name__, " Asset not present in wallet")