class AssetLimitExceeded(Exception):

	def __init__(self):
		self.msg = "The asset position in wallet is less then your sell quantity!"

	def __repr__(self):
		return self.msg

class AssetNotInWallet(Exception):

	def __init__(self):
		self.msg = "The asset has no position in your wallet!"

	def __repr__(self):
		return self.msg

class WalletHasNoThatAmount(Exception):

	def __init__(self):
		self.msg = "The wallet has no free resources to execute whitdraw"

	def __repr__(self):
		return self.msg

class WalletHasNoFreeResources(Exception):

	def __init__(self):
		self.msg = "The wallet has no free resources to execute operation!"

	def __repr__(self):
		return self.msg

class WalletEmpty(Exception):
	
	def __init__(self):
		self.msg = "The Wallet is empty!"

	def __repr__(self):
		return self.msg

