import json

from context.crypto.domain.entities.bitcoin import Bitcoin
from context.crypto.errors.crypto_errors import HasNoCrypto

class BackTest:

	DATA_FILE = "context/crypto/data/btc.json"

	def __init__(self, filename=DATA_FILE):
		data_file = open(filename, "r")
		self.data = json.load(data_file)["Time Series (Digital Currency Daily)"]

	def get_ticker(self, ticker, timestamp=None):
		if ticker == Bitcoin.ticker:
			if timestamp == None:
				currency = self.data[list(self.data.keys())[0]]
			else:
				currency = self.data[timestamp]

			return currency
		else:
			raise HasNoCrypto(ticker)

	def get_fees(self):
		raise NotImplementedError("Back Test Data Has no Fees!")