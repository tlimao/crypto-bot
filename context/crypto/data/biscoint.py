import json
from biscoint_api_python import Biscoint

class BiscointExchange:

	def __init__(self, api_key, api_secret_key):
		self.bsc = Biscoint(api_key, api_secret_key)

	def get_ticker(self, ticker):
		ticker = self.bsc.get_ticker()
		return ticker

	def get_fees(self):
		fees = bsc.get_fees()
		return fees