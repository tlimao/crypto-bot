import json
from datetime import datetime

class Crypto:

	def __init__(self, ticker, name, quote, high, low, vol, last, ask, bid, p_open, timestamp):
		self.ticker = ticker
		self.name = name
		self.quote = quote
		self.high = high
		self.low = low
		self.vol = vol
		self.last = last
		self.ask = ask
		self.bid = bid
		self.open = p_open
		self.timestamp = timestamp

	def __repr__(self):
		dict_copy = self.__dict__.copy()
		dict_copy["timestamp"] = datetime.strftime(dict_copy["timestamp"], "%Y-%m-%d %H:%M:%S")
		return json.dumps(dict_copy, indent=4)
