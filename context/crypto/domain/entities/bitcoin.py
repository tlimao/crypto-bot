from context.crypto.domain.entities.crypto import Crypto

class Bitcoin(Crypto):
    ticker = 'BTC'
    name = 'Bitcoin'

    def __init__(self, quote, high, low, vol, last, ask, bid, p_open, timestamp):
        super(Bitcoin, self).__init__(
            ticker=self.ticker,
            name=self.name,
            quote=quote,
            high=high,
            low=low,
            vol=vol,
            last=last,
            ask=ask,
            bid=bid,
            p_open=p_open,
            timestamp=timestamp)