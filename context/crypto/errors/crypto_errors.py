class HasNoCrypto(Exception):

	def __init__(self, ticker):
		self.msg = f"The Exchange has no Crypto {ticker}"

	def __repr__(self):
		return self.msg