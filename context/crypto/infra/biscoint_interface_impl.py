from datetime import datetime

from context.crypto.domain.entities.bitcoin import Bitcoin
from context.crypto.domain.interfaces.exchange_interface import ExchangeInterface

class BiscointInterfaceImpl(ExchangeInterface):

	def __init__(self, data):
		self.data = data

	def get(self, ticker):
		result_json = self.data.get_ticker(ticker)

		return Bitcoin(
			quote=result_json["quote"],
			high=round(result_json["high"], 2),
			low=round(result_json["low"], 2),
			vol=round(result_json["vol"], 8),
			last=round(result_json["last"], 2),
			buy=round(result_json["ask"], 2),
			sell=round(result_json["bid"], 2),
			p_open=None,
			timestamp=datetime.strptime(result_json["timestamp"].replace("Z",""), "%Y-%m-%dT%H:%M:%S.%f"))