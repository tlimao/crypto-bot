from datetime import datetime, timedelta

from context.crypto.domain.entities.bitcoin import Bitcoin
from context.crypto.domain.interfaces.exchange_interface import ExchangeInterface

class BackTestInterfaceImpl(ExchangeInterface):

	def __init__(self, data, initial_timestamp, final_timestamp):
		self.data = data
		self.initial_timestamp = initial_timestamp
		self.final_timestamp = final_timestamp
		self.now_timestamp = initial_timestamp
		self.delta = timedelta(days=1)

	def get(self, ticker):
		if self.now_timestamp <= self.final_timestamp:
			result_json = self.data.get_ticker(ticker, timestamp=datetime.strftime(self.now_timestamp, "%Y-%m-%d"))

			bitcoin = Bitcoin(
				quote="USD",
				high=round(eval(result_json["2b. high (USD)"]), 2),
				low=round(eval(result_json["3b. low (USD)"]), 2),
				vol=round(eval(result_json["5. volume"]), 8),
				last=round(eval(result_json["2b. high (USD)"]), 2),
				ask=round(eval(result_json["2b. high (USD)"]), 2),
				bid=round(eval(result_json["3b. low (USD)"]), 2),
				p_open=round(eval(result_json["1b. open (USD)"]), 2),
				timestamp=self.now_timestamp)

			self.now_timestamp = self.now_timestamp + self.delta

			return bitcoin

		else:
			raise Exception("Has no more data")
