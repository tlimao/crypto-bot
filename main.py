import yaml
from datetime import datetime

from context.crypto.data.biscoint import BiscointExchange
from context.crypto.infra.biscoint_interface_impl import BiscointInterfaceImpl

from context.crypto.data.backtest import BackTest
from context.crypto.infra.backtest_interface_impl import BackTestInterfaceImpl

if __name__ == "__main__":
	api_keys = None

	with open(r'config-dev.yaml') as file:
		api_keys = yaml.full_load(file)

	if api_keys:
		data = BiscointExchange(
			api_keys['api-keys'][0]['public-key'],
			api_keys['api-keys'][0]['secret-key'])

		biscoint_interface_impl = BiscointInterfaceImpl(data)

		result = biscoint_interface_impl.get("BTC")

		print(result)
	
	backtest = BackTest()
	backtest_interface_impl = BackTestInterfaceImpl(
		backtest,
		datetime.strptime("2018-04-01", "%Y-%m-%d"),
		datetime.strptime("2020-12-01", "%Y-%m-%d"))

	print(backtest_interface_impl.get("BTC"))
