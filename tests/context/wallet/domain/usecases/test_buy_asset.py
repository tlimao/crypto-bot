import unittest
from datetime import datetime

from context.wallet.domain.usecases.buy_asset import BuyAsset
from context.wallet.domain.entities.wallet import Wallet
from context.wallet.domain.entities.trade_operation import TradeOperation

class TestBuyAsset(unittest.TestCase):

	def setUp(self):
		self.wallet = Wallet(
			w_id="test_wallet_id",
			w_name="Test Wallet")

		self.buy_asset_usecase = BuyAsset()

	def test_buy_new_asset(self):
		""" Test a new Asset in Wallet """
		result = self.buy_asset_usecase.buy(
			wallet=self.wallet,
			ticker="Ticker",
			qnt=1,
			price=12000.34,
			timestamp=datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))

		self.assertIsInstance(result, TradeOperation)

	def test_buy(self):
		""" Test an Asset that is already on in Wallet """
		result_1 = self.buy_asset_usecase.buy(
			wallet=self.wallet,
			ticker="Ticker",
			qnt=1,
			price=12000.34,
			timestamp=datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))

		self.assertIsInstance(result_1, TradeOperation)

		result_2 = self.buy_asset_usecase.buy(
			wallet=self.wallet,
			ticker="Ticker",
			qnt=1,
			price=16000.34,
			timestamp=datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))

		asset = self.wallet.get_asset("Ticker")

		self.assertIsInstance(result_2, TradeOperation)
		
		self.assertEqual(asset.qnt, result_1.qnt + result_2.qnt)

if __name__ == '__main__':
	unittest.main()