import unittest
from datetime import datetime

from context.wallet.domain.usecases.sell_asset import SellAsset
from context.wallet.domain.usecases.buy_asset import BuyAsset
from context.wallet.domain.entities.wallet import Wallet
from context.wallet.domain.entities.trade_operation import TradeOperation

class TestSellAsset(unittest.TestCase):

	def setUp(self):
		self.wallet = Wallet(
			w_id="test_wallet_id",
			w_name="Test Wallet")

		self.buy_asset_usecase = BuyAsset()

		result = self.buy_asset_usecase.buy(
			wallet=self.wallet,
			ticker="Ticker",
			qnt=2,
			price=12000.34,
			timestamp=datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))

		self.sell_asset_usecase = SellAsset()

	def test_buy(self):
		""" Test an Asset that is already on in Wallet """
		asset = self.wallet.get_asset("Ticker")
		qnt_0 = asset.qnt

		result = self.sell_asset_usecase.sell(
			wallet=self.wallet,
			ticker="Ticker",
			qnt=1,
			price=12000.34,
			timestamp=datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))

		self.assertIsInstance(result, TradeOperation)

		asset = self.wallet.get_asset("Ticker")
		qnt_1 = asset.qnt

		self.assertEqual(qnt_0, qnt_1 + result.qnt)

if __name__ == '__main__':
	unittest.main()