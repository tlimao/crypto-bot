import os
import time
from datetime import datetime

from examples.bot_1 import BotSupportResistance
from context.wallet.domain.entities.wallet import Wallet
from context.strategy.domain.entities.support_resistance import SupportResistance
from context.crypto.data.backtest import BackTest
from context.crypto.infra.backtest_interface_impl import BackTestInterfaceImpl

if __name__ == "__main__":
	strategy = SupportResistance()
	repository = BackTestInterfaceImpl(
		BackTest(),
		datetime.strptime("2018-04-01", "%Y-%m-%d"),
		datetime.strptime("2020-12-01", "%Y-%m-%d"))

	wallet = Wallet("wallet_1", "Wallet Test", resources=1000.00)

	bot_1 = BotSupportResistance(
		"1",
		"Bot to Support Resistance Strategy",
		repository,
		strategy,
		wallet)

	try:
		bot_1.start()

	except Exception as e:
		print(e)
		bot_1.stop()
